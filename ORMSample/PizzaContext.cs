﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMSample.Model;

namespace ORMSample
{
    public class PizzaContext : DbContext
    {
        public PizzaContext() : base("PizzaConnection")
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<PizzaContext>());
        }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<ContactInfo> ContactInfos { get; set; }

        public DbSet<Address> Addresses { get; set; }
    }
}
