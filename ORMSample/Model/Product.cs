﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSample.Model
{
    public class Product : BaseModel
    {
        public List<Order> Orders { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

    }
}
