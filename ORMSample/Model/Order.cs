﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSample.Model
{
    public class Order : BaseModel
    {
        public User User { get; set; }

        public List<Product> Products { get; set; }

        public Address Address { get; set; }

        public ContactInfo ContactInfo { get; set; }
    }
}
