﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORMSample.Model
{
    public class User : BaseModel
    {
        public List<Order> Orders { get; set; }

        public ContactInfo ContactInfo { get; set; }

        public List<Address> Addresses { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthdBay { get; set; }

        public Roles Role { get; set; }
    }
}
