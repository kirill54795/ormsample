﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORMSample.Model;

namespace ORMSample
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaContext context = new PizzaContext();

            //insert
            var product = new Product();
            product.Name = "FatherPizza";
            product.Price = 123;

            context.Products.Add(product);
            context.SaveChanges();
            //update
            product = context.Products.Find(1);

            //select
            var res = context.Products.Where(p => p.Price > 20).ToString();
            Console.WriteLine(res);
            Console.ReadLine();
            product.Name = "Product";
            context.SaveChanges();

            //delete
            context.Products.Remove(product);
            context.SaveChanges();
            


        }
    }
}
