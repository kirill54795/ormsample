﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ORMSample;
using ORMSample.Model;

namespace PizzaShopTest
{
    [TestClass]
    public class PizzaContextTest
    {
        /// <summary>
        /// 4 CRUD operation
        /// </summary>
        
        [TestMethod]
        public void InsertTest()
        {
            PizzaContext context = new PizzaContext();

            Order order = new Order
            {
                Address = new Address() {Building = "12", FlatNumber = "444", Street = "Kuprevicha", City = "Minsk"},
                ContactInfo = new ContactInfo() {Email = "Raman@mail.com", Name = "Raman", PhoneNumber = "1213456789"},
                Products = new List<Product>() {new Product() {Name = "CheesPizza", Price = 10}}
            };
            context.Orders.Add(order);
            context.SaveChanges();

            var actual = context.Orders.Find(1);
            Assert.IsNotNull(context.Products.Find(1));
        }

        [TestMethod]
        public void UpdateTest()
        {
            PizzaContext context = new PizzaContext();

            Order order = new Order
            {
                Address = new Address() {Building = "12", FlatNumber = "444", Street = "Kuprevicha", City = "Minsk"},
                ContactInfo = new ContactInfo() {Email = "Raman@mail.com", Name = "Raman", PhoneNumber = "1213456789"},
                Products = new List<Product>() {new Product() {Name = "CheesPizza", Price = 10}}
            };
            context.Orders.Add(order);
            context.SaveChanges();

            order.Products = new List<Product>() {new Product() {Name = "SaladWithCucumber", Price = 5}};
            order.Address = new Address() {Building = "100500", FlatNumber = "1234", Street = "VasyaStreet", City = "Minsk"};
            order.ContactInfo = new ContactInfo() {Email = "Vasya@mail.com", Name = "Vasya", PhoneNumber = "111111111"};
            
            context.Orders.Add(order);
            context.SaveChanges();

            order = context.Orders.Find(2);

            order.ContactInfo.Name = "Petya";
            context.SaveChanges();

            var expected = order.ContactInfo.Name;
            Assert.IsNotNull(context.Products.Find(1));
            Assert.AreEqual(expected, context.Orders.Find(2).ContactInfo.Name);
        }

        [TestMethod]
        public void DeleteTest()
        {
            PizzaContext context = new PizzaContext();

            Order order = new Order
            {
                Address = new Address() {Building = "12", FlatNumber = "444", Street = "Kuprevicha", City = "Minsk"},
                ContactInfo = new ContactInfo() { Email = "Vasya@mail.com", Name = "Petya", PhoneNumber = "111111111" },
                Products = new List<Product>() {new Product() {Name = "CheesPizza", Price = 10}}
            };
            context.Orders.Add(order);
            context.SaveChanges();

            order.Products = new List<Product>() {new Product() {Name = "SaladFromChief", Price = 50}};
            order.Address = new Address() {Building = "100", FlatNumber = "1", Street = "VasyaStreet", City = "Minsk"};
            order.ContactInfo = new ContactInfo() {Email = "Vasya@mail.com", Name = "Vasya", PhoneNumber = "111111111"};
            context.Orders.Add(order);
            context.SaveChanges();

            order.Products = new List<Product>() {new Product() {Name = "SaladWithCucumber", Price = 5}};
            order.Address = new Address() {Building = "100500", FlatNumber = "1234", Street = "VasyaStreet", City = "Minsk"};
            order.ContactInfo = new ContactInfo() {Email = "Vasya@mail.com", Name = "Vasya", PhoneNumber = "111111111"};
            context.Orders.Add(order);
            context.SaveChanges();

            var vasyaSelection = context.Orders.Where(o => o.ContactInfo.Name == ("Vasya"));
            context.Orders.RemoveRange(vasyaSelection);
            context.SaveChanges();

            Assert.IsNull(context.Orders.FirstOrDefault(o => o.ContactInfo.Name == "Vasya"));
            ///При добавлении записи надо после Инициализации объекта каждый раз вызывать М-од SaveChanges? Или Каждый раз создавать экзепляр?
            ///Почему в дебаге LINQ не сразу возвращает значение а после 2 или 3 шагов
        }

        [TestMethod]
        public void SelectTest()
        {
            PizzaContext context = new PizzaContext();

            Order order = new Order
            {
                Address = new Address() {Building = "12", FlatNumber = "444", Street = "Kuprevicha", City = "Minsk"},
                ContactInfo = new ContactInfo() { Email = "Vasya@mail.com", Name = "Petya", PhoneNumber = "111111111" },
                Products = new List<Product>() {new Product() {Name = "CheesPizza", Price = 10}}
            };
            context.Orders.Add(order);
            context.SaveChanges();

            order.Products = new List<Product>() {new Product() {Name = "SaladFromChief", Price = 50}};
            order.Address = new Address() {Building = "100", FlatNumber = "1", Street = "VasyaStreet", City = "Minsk"};
            order.ContactInfo = new ContactInfo() {Email = "Vasya@mail.com", Name = "Vasya", PhoneNumber = "111111111"};
            context.Orders.Add(order);
            context.SaveChanges();

            order.Products = new List<Product>() {new Product() {Name = "SaladWithCucumber", Price = 5}};
            order.Address = new Address() {Building = "100500", FlatNumber = "1234", Street = "VasyaStreet", City = "Minsk"};
            order.ContactInfo = new ContactInfo() {Email = "Vasya@mail.com", Name = "Vasya", PhoneNumber = "111111111"};
            context.Orders.Add(order);
            context.SaveChanges();

            var vasyaSelection = context.Orders.Where(o => o.ContactInfo.Name == ("Vasya"));
            context.Orders.RemoveRange(vasyaSelection);
            context.SaveChanges();

            Assert.IsNull(context.Orders.FirstOrDefault(o => o.ContactInfo.Name == "Vasya"));
            ///При добавлении записи надо после Инициализации объекта каждый раз вызывать М-од SaveChanges? Или Каждый раз создавать экзепляр?
            ///Почему в дебаге LINQ не сразу возвращает значение а после 2 или 3 шагов
        }
    }
}
